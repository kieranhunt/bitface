#!/usr/bin/python

from Tkinter import *
import sqlite3 as lite
import sys
import datetime
import os
from btsync import BTSync
import time
import shutil

class MainWindow:

    def __init__(self, master):
        """
        Constructor for the main window.
        Defines the location and functions each of the items in the main window.

        Keyword arguments:
        master -- The window from which this window spawns. Its parent.
        """

        self.messages = []
		
        #Top Buttons

        self.followButton = Button(master, text="Follow", command=self.newFollow)
        self.followButton.grid(row=0, column=0)
         
        self.followButton = Button(master, text="Refresh", command=self.refresh)
        self.followButton.grid(row=0, column=1)
        
        self.followButton = Button(master, text="Unfollow", command=self.unfollow)
        self.followButton.grid(row=0, column=2) 

        
        
        self.options = []
        self.refreshOptions(master)

        self.variable.set(self.options[0])
        self.w = apply(OptionMenu, (master, self.variable) + tuple(self.options))
        self.w.grid(row=1, column=0, columnspan=3)
        
        #Message Panel

        self.frame = Frame(master, width=600, height=480)
        self.frame.grid(row=2, column=0, columnspan=3)
        
        #Bottom Buttons
        
        self.followButton = Button(master, text="New Group", command=self.showNewGroup)
        self.followButton.grid(row=3, column=0)

        self.followButton = Button(master, text="Post", command=self.showNewPost)
        self.followButton.grid(row=3, column=1)
        
        self.followButton = Button(master, text="Following", command=self.showFollowingList)
        self.followButton.grid(row=3, column=2)
        
        
        #Bottom Info Panel
        
        with con:
            cur = con.cursor()
            cur.execute('SELECT SQLITE_VERSION()')
            data = cur.fetchone()
        
            self.SQLiteVersionLabel = Label(master, text="SQLite DB version: %s" % data, anchor="w")
            self.SQLiteVersionLabel.grid(row=4, column=0)

            speed = btsync.get_speed()["download"]

            self.OSVersionLabel = Label(master, text="Speed: %s" % speed)
            self.OSVersionLabel.grid(row=4, column=2)
					
        self.refresh()
        
    def refresh(self):
        """
        Refreshes the posts displayed within the main window.
        Searches through all of the directories and finds all of the files
        with .txt extensions. Searches through both personal and other users'
        directories.
        Also reads out the name from the file so that the poster can be identified.

        Keyword arguments:
        None
        """
        self.messages = []

        for child in self.frame.winfo_children():
            child.destroy()

        print self.w.cget("text").split(" - ")[0]
        for path, subdirs, files in os.walk(r'data'):
            for subdir in subdirs:
                for subpath, subdirs, subfiles in os.walk(r'data/'+subdir):
                    if (subdir == self.w.cget("text").split(" - ")[0]):
                        for filename in subfiles:                        
                            if (filename.endswith('.txt')):                       
                                self.messages.append(open(subpath+'/'+filename, 'r').read())
        self.messagesNew = []

        for self.m in self.messages:
            self.messagesNew.append(self.m.split('\n')[0] + " - " + self.m.split('\n')[1])

        for index, message in enumerate(self.messagesNew):
            self.messagesNew[index] = [message, Label(self.frame, text=message)]
            self.messagesNew[index][1].grid(row=index)
                

    def newFollow(self):
        """
        Spawns a new follow window and names it.

        Keyword arguments:
        None
        """        
        addNewFollowWindow = Toplevel(root)
        addNewFollowWindow.wm_title('Follow a new group')

        followWindow = FollowWindow(addNewFollowWindow)

    def unfollow(self):
        """
        Spawns an unfollow window and names it.

        Keyword arguments:
        None
        """
        removeFollowWindow = Toplevel(root)
        removeFollowWindow.wm_title('Remove a group')

        removeWindow = RemoveWindow(removeFollowWindow)
      
    def showFollowingList(self):
        """
        Spawns the following list window and names it.

        Keyword arguments:
        None
        """
        showFollowingWindow = Toplevel(root)
        showFollowingWindow.wm_title('Remove a group')

        followingWindow = FollowingWindow(showFollowingWindow)

    def showNewGroup(self):
        """
        Spawns a new group window and names it.

        Keyword arguments:
        None
        """
        newGroupWindow = Toplevel(root)
        newGroupWindow.wm_title('Create a new group')

        groupWindow = addNewGroupWindow(newGroupWindow)

    def showNewPost(self):
        """
        Spawns a new post window and names it.

        Keyword arguments:
        None
        """
        addshowNewPost = Toplevel(root)
        addshowNewPost.wm_title('')

        showNewPostWindow = ShowNewPostWindow(addshowNewPost)

    def refreshOptions(self, master):
        """
        Refresh the list of groups to chose from.

        Keyword arguments:
        master -- The GUI from which this window spawns.
        """
        cur = con.cursor()
        self.query = cur.execute('SELECT * from following;')
        self.variable = StringVar(master)
        for i, self.row in enumerate(self.query):
            self.options.append(self.row[0] + ' - ' + self.row[1])     

class FollowWindow:

    def __init__(self, master):
        """
        The constructor for the follow window. Places all of the items on the GUI.

        Keyword arguments:
        master -- The parent GUI from which the window will be spawned.
        """
        self.secretFieldLabel = Label(master, text='secret')
        self.secretFieldLabel.grid(row=0, column=0)
        self.secretField = Entry(master)
        self.secretField.grid(row=0, column=1, columnspan=2)

        self.aliasField = Entry(master)
        self.aliasField.grid(row=1, column=1, columnspan=2)
        self.aliasFieldLabel = Label(master, text='alias (optional)')
        self.aliasFieldLabel.grid(row=1, column=0)

        self.followButton = Button(master, text="Follow", command=self.followNew)
        self.followButton.grid(row=2, column=2)

    def followNew(self):
        """
        Handles adding new subscriptions.
        Checks to see if the subscription already exists.
        If not it creates the folder,
        adds the secret to BitTorrent Sync and
        inserts the record into the database.

        Keyword arguments:
        None
        """      
        self.secret = self.secretField.get()
        self.alias = self.aliasField.get()
  
        cur = con.cursor()
        cur.execute('SELECT count(*) from following where ID is \"' + self.secret + '\";')
        numAlready  = cur.fetchone()
        print numAlready

        if (numAlready[0] != 0):
            print "Group already followed"
            return
        
        newFolderPath = os.getcwd() + r'/data/' + self.secret
        print newFolderPath
        if not os.path.exists(newFolderPath): os.makedirs(newFolderPath) 
	
        outcome = btsync.add_folder(newFolderPath, self.secret)
        
        if (outcome["error"] != 0):
            print "BTSync Error: ", outcome["error"]
            return
        
        cur = con.cursor()
        query = 'INSERT INTO following values (\"' + self.secret + '\", \"' + self.alias + '\");';
        print query
        cur.execute(query) #not working, might need a commit transaction.
        con.commit()

class RemoveWindow:

    def __init__(self, master):
        """
        The constructor the for remove window.
        places each of the followers on the window and adds
        buttons to each one.

        Keyword arguments:
        master -- The parent GUI from which this one spawns.
        """
        cur = con.cursor()
        cur.execute('SELECT * from following;')
        rows = cur.fetchall()
        self.unFollowButtons = []

        self.selected="";

        for i, row in enumerate(rows):
            self.IDLabel = Label(master, text=row[0])
            self.IDLabel.grid(row=i, column=0)
        
            self.AliasLabel = Label(master, text=row[1])
            self.AliasLabel.grid(row=i, column=1)

            self.unFollowButtons.append(Button(master, text="Unfollow", command=lambda:self.removeFollow(row[0])))
            self.unFollowButtons[i].grid(row=i, column=2)

    def removeFollow(self, ID):
        """
        Removes a follower.

        Keyword arguments:
        ID -- The identification secret of the subscription to be removed.
        """
        cur = con.cursor()
        query = 'DELETE FROM following WHERE ID=\"' + ID + '\";'
        cur.execute(query)
        con.commit()

        delFolderPath = os.getcwd() + r'/data/' + ID

        if os.path.exists(delFolderPath): shutil.rmtree(delFolderPath)

        outcome = btsync.remove_folder(ID)

        print "DELETED: " + delFolderPath


class addNewGroupWindow:

    def __init__(self, master):
        """
        The constructor to create the add new group window.

        Keyword arguments:
        master -- the GUI from which the window spawns.        
        """

        self.aliasField = Entry(master)
        self.aliasField.grid(row=1, column=1, columnspan=2)
        self.aliasFieldLabel = Label(master, text='alias (optional)')
        self.aliasFieldLabel.grid(row=1, column=0)

        self.addGroupButton = Button(master, text="Add Group", command=self.newGroup)
        self.addGroupButton.grid(row=2, column=2)

    def newGroup(self): 
        """
        Adds a new group by creating a secret,
        making a folder,
        and adding it to the database.

        Keyword arguments:
        None       
        """   
        self.alias = self.aliasField.get()

        self.newSecret = btsync.get_secrets()["read_write"]
        
        newFolderPath = os.getcwd() + r'/data/' + self.newSecret
        print newFolderPath
        if not os.path.exists(newFolderPath): os.makedirs(newFolderPath) 
    
        outcome = btsync.add_folder(newFolderPath, self.newSecret)
        
        if (outcome["error"] != 0):
            print "BTSync Error: ", outcome["error"]
            return
        
        cur = con.cursor()
        query = 'INSERT INTO following values (\"' + self.newSecret + '\", \"' + self.alias + '\");';
        print query
        cur.execute(query) #not working, might need a commit transaction.
        con.commit()

class ShowNewPostWindow:

    def __init__(self, master):
        """
        Constructor for the new post window. Creates the GUI items.

        Keyword arguments:
        master -- the GUI from which this window spawns. Its parent window.
        """
        self.postField = Entry(master)
        self.postField.grid(row=0, column=0, columnspan=2)

        self.makePostButton = Button(master, text="Post", command = self.newPost)
        self.makePostButton.grid(row=1, column=1)

        self.options = []
        self.refreshOptions(master)

        self.variable.set(self.options[0])
        self.w = apply(OptionMenu, (master, self.variable) + tuple(self.options))
        self.w.grid(row=1, column=0)

    def newPost(self):
        """
        Creates a new post by writing the content the correct text file.

        Keyword arguments:
        None.
        """
        self.newfileName = os.getcwd() + r'/data/' + self.w.cget("text").split(" - ")[0] + '/' + str(int(time.time())) + ".txt"
        print self.newfileName
        self.newfile = open(self.newfileName, 'w')
        self.newfile.write(name + '\n'+ self.postField.get())
        self.newfile.close()

    def refreshOptions(self, master):
        """
        Refresh the list of groups subscribed to.

        Keyword arguments:
        None
        """
        cur = con.cursor()
        self.query = cur.execute('SELECT * from following;')
        self.variable = StringVar(master)
        for i, self.row in enumerate(self.query):
            self.options.append(self.row[0] + ' - ' + self.row[1])

class FollowingWindow:

    def __init__(self, master):
        """
        Constructor for the following window. Creates the GUI items.

        Keyword arguments:
        master -- the GUI from which this window spawns. Its parent window.
        """
        self.options = []
        self.refreshOptions(master)

        self.variable.set(self.options[0])
        self.w = apply(OptionMenu, (master, self.variable) + tuple(self.options))
        self.w.grid(row=0, column=0, columnspan=3)

        self.frame = Frame(master, width=600, height=480)
        self.frame.grid(row=1, column=0, columnspan=3)

        self.makePostButton = Button(master, text="View Followers", command =lambda:self.showFollowers(self.w.cget("text").split(" - ")[0], master))
        self.makePostButton.grid(row=2, column=2)

    def showFollowers(self, ID, master):
        """
        List the followers based on the share.

        Keyword arguments:
        master -- the GUI from which this window spawns. Its parent window.
        """

        for child in self.frame.winfo_children():
            child.destroy()
            
        print ID
        self.followersjson = btsync.get_folder_peers(ID)

        for i, follower in enumerate(self.followersjson):
            self.IDLabel = Label(master, text=" - " + self.followersjson[i]["name"])
            self.IDLabel.grid(row=i+1, column=0)

    def refreshOptions(self, master):
        """
        Refreshed the list of groups subscribed to.
        Keyword arguments:
        None.
        """
        cur = con.cursor()
        self.query = cur.execute('SELECT * from following;')
        self.variable = StringVar(master)
        for i, self.row in enumerate(self.query):
            self.options.append(self.row[0] + ' - ' + self.row[1])

con = lite.connect('BitFace.db')
name = "Kieran Hunt"
btsync = BTSync()

root = Tk()
root.wm_title("BitFace")

app = MainWindow(root)

root.resizable(width=FALSE, height=FALSE)
root.mainloop()
root.destroy()